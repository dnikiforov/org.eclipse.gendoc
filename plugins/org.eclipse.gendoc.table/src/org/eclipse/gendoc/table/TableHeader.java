/**
 */
package org.eclipse.gendoc.table;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Header</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.gendoc.table.TablePackage#getTableHeader()
 * @model
 * @generated
 */
public interface TableHeader extends Row {
} // TableHeader
