/**
 */
package org.eclipse.gendoc.table.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.gendoc.table.TableHeader;
import org.eclipse.gendoc.table.TablePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Header</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TableHeaderImpl extends RowImpl implements TableHeader {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TableHeaderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TablePackage.Literals.TABLE_HEADER;
	}

} //TableHeaderImpl
