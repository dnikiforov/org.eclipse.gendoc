package org.eclipse.gendoc.document.parser.pptx;

public class PPTXRef {
	public PPTXRef(String xpathSel) {
		this(null,xpathSel);
	}

	public PPTXRef(String partPath, String xpathSel) {
		this.partPath = partPath;
		this.xpathSelector = xpathSel;
	}
	
	public String toString() {
		if (partPath == null)
			return xpathSelector.replace('[', '{').replace(']','}');
		return "{"+partPath+"}:"+xpathSelector.replace('[', '{').replace(']','}');
	}
	
	public static final PPTXRef parse(String txt) {
		String[] parts = txt.split("\\:");
		if (parts[0].startsWith("{") && parts[0].endsWith("}")) {
			return new PPTXRef(parts[0].substring(1, parts[0].length()-1), 
				txt.substring(parts[0].length()+1).replace('{', '[').replace('}',']'));
		} else {
			return new PPTXRef(txt.replace('{', '[').replace('}',']'));
		}
	}
	
	public final String partPath;
	public final String xpathSelector; 
}
