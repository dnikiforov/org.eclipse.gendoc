package org.eclipse.gendoc.bundle.acceleo.gmf.service;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.gendoc.documents.IImageManipulationService;
import org.eclipse.gendoc.documents.IImageManipulationServiceFactory;
import org.eclipse.gendoc.services.GendocServices;
import org.eclipse.gendoc.services.IGendocDiagnostician;
import org.eclipse.gendoc.services.IService;
import org.eclipse.gendoc.tags.handlers.Activator;
import org.eclipse.gmf.runtime.notation.Diagram;

public interface IDiagramRenderer extends IService {
	public enum FileFormat {
		PNG, JPEG, GIF, BMP, JPG, SVG, EMF;
		
		public FileFormat getExtension(){
			IImageManipulationServiceFactory imageManipulationServiceFactory = GendocServices.getDefault().getService(IImageManipulationServiceFactory.class);
        	IImageManipulationService imageManipulationService = imageManipulationServiceFactory.getService(name().toLowerCase()) ;
			return transformToFormat(imageManipulationService.renameExtension(name()));
		}
		
		public String getFullExtension() {
        	return "." + getExtension().name().toLowerCase();
		}

		public static FileFormat transformToFormat(String ext) {
			FileFormat format;
			try {
				format = FileFormat.valueOf(ext.toUpperCase());
				return format;
			} catch (IllegalArgumentException e) {
				IGendocDiagnostician diagnostician = GendocServices.getDefault()
						.getService(IGendocDiagnostician.class);
				String message = "The format " + ext + " is not supported";
				diagnostician.addDiagnostic(new BasicDiagnostic(Diagnostic.ERROR,
						Activator.PLUGIN_ID, 0, message, null));
				return FileFormat.valueOf(ext);
			}

		}
	}

	/**
	 * Render the diagram into a image.
	 * 
	 * @param diagram the diagram to render
	 * @param visibleElements a list with the visible elements in the diagram
	 * @param destination the path where the image will be stored
	 * @param imageFormat the format of the image
	 * @param monitor the progress monitor
	 * @return a list with the top level visible 
	 * @throws CoreException
	 * 
	 */
	public List<EditPart> renderDiagram(Diagram diagram, List<EObject> visibleElements, IPath destination, FileFormat imageFormat, NullProgressMonitor monitor) throws CoreException;
}
